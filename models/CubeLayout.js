const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const SchemaTypes = mongoose.SchemaTypes;

module.exports = function (name) {

  var cubeLayoutSchema = new Schema({
    CubeID: {
      type: String,
      required: true
    },
    layoutName: String,
    dataParams: [],
    graphParams: [],
  }, {
    timestamps: true // for createdAt, updatedAt atributes
  })

  return mongoose.model(name, cubeLayoutSchema, 'cubelayout');
}

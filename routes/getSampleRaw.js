
const logger = require("../utils/Logger")

module.exports = function (request, response) {
    var cube = response.locals.cube;
    var cubeId = cube.CubeID;
    var models = request.app.locals.models;
    var resp = {};
    var data = [];

    //get the mandatory query params
    var parameter = request.query.param;
    var start = parseInt(request.query.start);
    var count = parseInt(request.query.count);

    //optional parameter feeder
    var feeder = request.query.feeder;

    //feeder is an optional param, if feeder is given then look for the parameter in the feeder!
    if(parameter === undefined || start === undefined || count === undefined) {
        logger.error("one or more of the Mandatory parameters are missing for the intp request!");
        response.status(400).send();
    }

    //we now have all the params! look in the db for the data with cubeId and params
    models.cubeData.find({
    "Busbar.ts" : {
        $lte : start
    },
    "CubeID" : cubeId
    }).sort({"Busbar.ts" : -1}).limit(count).exec((err,dataArray) => {

        if(err) {
            logger.error(err);
            response.status(500).send();
        }

        if(dataArray === null) {
            logger.error("find query returned null");
            response.status(404).send();
        }
        //data exsists
        dataArray.forEach(element => {
            var json = {};
            //get the value of the parameter from the Busbar json
            if(feeder !== undefined) {
                var feederArr = element["Feeders"];
                var found = 0;
                feederArr.forEach((feed) => {
                    //apparently u cant break in foreach loop!!
                    if(found === 1) {
                    }

                    if(feed.Name === feeder) {
                        if(feed[parameter] !== undefined) {
                            json["value"] = feed[parameter];
                        } else {
                            json["value"] = null;
                        }
                        json["ts"] = feed["ts"];
                        data.push(json);
                        found =1;
                    }
                })

                if(found !== 1) {
                    //send null data if feeder not found
                    json["value"] = null;
                    json["ts"] = feederArr[0]["ts"];
                    data.push(json);
                }
            } else {
                if(element.Busbar[parameter] !== undefined) {
                    json["value"] = element.Busbar[parameter];
                }else {
                    json["value"] = null;
                }
                json["ts"] = element.Busbar["ts"];
                data.push(json);
            }
        });

        resp["data"] = data;
        response.status(200).send(resp);
    });

  }
const cronJob             = require('cron').CronJob;
const moment              = require('moment');

var models                = require("../models");

module.exports = new cronJob({
  cronTime    : "0 */2 * * * *", // run every 2 minutes
  onTick      : async function () {
    var cubes = await models.Cube.find();
    
    cubes.forEach(async function (cube) {
      var now = moment();
      var lastComm = moment(cube.lastComm);

      var diff = now.diff(lastComm, 'seconds');

      if (diff > cube.idleTimeout) {
        // update cube's offline status
        cube.isOffline = true;
        await cube.save();
      }
    })
  },
  start       : false // won't start automatically on declaration
})
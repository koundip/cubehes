const mongoose						= require('mongoose');

const Schema 							= mongoose.Schema;
const SchemaTypes 				= mongoose.SchemaTypes;

module.exports						= function (name) {

	var schema							= new Schema({
	CubeID  : {
        type                : String,
        required            : true,
    },
    Busbar : {
        type : JSON,
        required : true
    },
    Feeders : {
        type:Array,
        required : true
    }
    })
    schema.index({ CubeID: 1, "Busbar.ts": 1 },{'unique' : true});

	return mongoose.model(name, schema);
}

module.exports = function(request,response) {
    var cube = response.locals.cube;
    var cubeId = cube.CubeID;
    var locals = request.app.locals;

    //use the cubeId to iterate over the array to find the event
    for(var i=0;i < locals.events.length;i++) {
        var element = locals.events[i];
        if(element["CubeId"] == cubeId) {
            response.status(200).send(locals.events[i]);
            return;
        }
    }
    response.status(404).send();
};
const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const SchemaTypes = mongoose.SchemaTypes;

module.exports = function (name) {

    var schema = new Schema({
        CubeID: {
            type: String,
            required: true,
        },
        Temp10: {
            type: JSON,
            required: true
        },
        // Busbar : {
        //     type : JSON,
        //     required : true
        // },
        // Feeders : {
        //     type:Array,
        //     required : true
        // }
    })
    //commenting this for now but eventually the ts should be in the top json so that we dotn have to 
    // look into Busbar or Temp10 to get the ts
    //Then make a unique key with the ts and cubeID!!
    //    schema.index({ CubeID: 1, "Busbar.ts": 1 },{'unique' : true});

    return mongoose.model(name, schema);
}
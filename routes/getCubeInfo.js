const moment              = require('moment');

module.exports = function (request, response) {
  var cube = response.locals.cube;
  var retObj = Object.assign(
    {
      error   : false,
      message : "Success"
    },
    {
      CubeID      : cube.CubeID,
      enabled     : cube.enabled,
      busbarParams: cube.busbarParams,
      feederParams: cube.feederParams,
      feeders     : cube.feeders,
      description : cube.description,
      coordinates : cube.coordinates,
      isOffline   : cube.isOffline,
      temp10Params: cube.temp10Params,
      lastComm    : moment(cube.lastComm).unix()
    }
  )

  response.send(retObj);
}
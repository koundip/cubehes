const express 						= require('express');
const router              = express.Router();

// load routes
var getCubeInfo           = require("./getCubeInfo");
var getSampleRaw           = require("./getSampleRaw");
var getSampleRangeRaw           = require("./getSampleRangeRaw");
var getLastEvent           = require("./getLastEvent");
var getLastData            = require("./getLastData");
var cubeLayout            = require("./cubeLayout");


router.get("/",           getCubeInfo);
///sampleraw?param=:param&start=:start&count=:count
router.get("/sampleraw",getSampleRaw);

///sampleraw?param=:param&start=:start&count=:count&end=:end
router.get("/samplerangeraw",getSampleRangeRaw);

//lastEvent
router.get("/lastevent",getLastEvent);
//lastData
router.get("/lastdata",getLastData);

//getCubeLayout
router.get('/getcubelayout',cubeLayout.getCubeLayout);

router.post('/addcubelayout', cubeLayout.cubeLayoutCreate);

router.post('/adddataparams', cubeLayout.addCubeDataParams);

router.post('/addgraphparams', cubeLayout.addCubeGraphParams)



module.exports            = router;
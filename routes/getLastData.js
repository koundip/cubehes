
module.exports = function(request,response) {
    var cube = response.locals.cube;
    var cubeId = cube.CubeID;
    var locals = request.app.locals;

    //use the cubeId to iterate over the array to find the event
    for(var i=0;i < locals.data.length;i++) {
        if(locals.data[i]["CubeID"] == cubeId) {
            response.status(200).send(locals.data[i]);
            return;
        }
    }
    response.status(404).send();
};
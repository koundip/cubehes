const config = require("../config");
const moment = require("moment");
const chalk = require("chalk");
var levelarr = ["error","warning","info","debug"];
var level = levelarr.indexOf(config.loglevel);

if(level === -1) {
    level = 0;
}

//'error' = 0
//'warning'= 1
//'info' = 2
//'debug' = 3

var getTime = () => {
    return moment().local().format('YYYY-MM-DD HH:mm:ss');
}
module.exports.debug = (message) => {
    if(level === 3) {
        console.log(chalk.white("[DBG] " + getTime() + " : " + message));
    }
}
module.exports.info = (message) => {
    if(level >= 2) {
        console.log(chalk.yellow("[INF] " + getTime() + " : " + message));
    }
}
module.exports.warning = (message) => {
    if(level >= 1) {
        console.log(chalk.orange("[WRN] " + getTime() + " : " + message));
    }
}
module.exports.error = (message) => {
        console.log(chalk.red("[ERR] " + getTime() + " : " + message));
}
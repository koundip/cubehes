module.exports = {
  server    : {
    port    : 7000
  },
  db        : {
    host    : 'localhost',
    database: 'cube-hes',
    port    : 27017,
    user    : 'cube',
    password: 'hes'
  },
  loglevel  : 'debug'
}

const express 						= require('express');
const bodyParser 					= require('body-parser');
const morgan							= require('morgan');
const moment              = require('moment');
const mqtt                = require('mqtt');
const logger              = require("./utils/Logger");

// load config
const config              = require("./config");

// load models
var models								= require("./models");

var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({'type':'*/json'}));

// configure app for : logging , cors
app.use(morgan("dev",{
  skip: function (req, res) {
    return res.statusCode >= 400
  },
  "stream": {
    write: function(str) { logger.info(str) }
  }
}));
app.use(morgan("dev",{
  skip: function (req, res) {
    return res.statusCode < 400
  },
  "stream": {
    write: function(str) { logger.error(str) }
  }
}));
// app.locals will be available in each
// request as "request.app.locals"
// passing all reusable information
app.locals.config         = config;
app.locals.models         = models;
app.locals.events         = [];
app.locals.data           = [];

// load cube Health Service
var cubeHealth            = require("./utils/cubeHealth");

// load any middleware
var verifyCube            = require("./utils/verifyCube");

// load routes
var routes                = require("./routes");

// declare cube-specific routes
app.use(
  "/api/:cubeId",
  verifyCube,
  routes
);

// 404
app.use(
  function (request, response) {
    response
    .status(404)
    .send({
      error   : true,
      message : "Route not found"
    })
  }
)
//mqtt stuff!
var client = mqtt.connect("mqtt://staging-blr.tekpea.com:8888");

//process Function
var processMqttData     = require('./mqttUtils/processData.js');

client.on('connect',()=>{
  client.subscribe('blr/cube/data',() =>{
    logger.info('Subscribed to blr/cube/data on staging bangalore');
  });
  client.subscribe('blr/cube/event',() =>{
    logger.info('Subscribed to blr/cube/event on staging bangalore');
  });
});

client.on('message', (topic,data) =>{
  processMqttData(topic,app.locals,data);
});

client.on('error',() =>{
  logger.error(error);
})

// connect to database
models.mongoose.connect(
  'mongodb://'
  + config.db.user
  + ':'
  + config.db.password
  + '@'
  + config.db.host
  + ':'
  + config.db.port
  + '/'
  + config.db.database,
  {
    useNewUrlParser : true
  }
)

.then(() => {
	app.listen(
		config.server.port,
		function () {
			logger.info("Server started at", moment().toString());
      logger.info("Server running on port", config.server.port);
      
      // launch Cube Health service
      cubeHealth.start();
		}
	);
})
.catch(error => {
	logger.error(`Error connecting to Database. Error= ${error.message}`);
})
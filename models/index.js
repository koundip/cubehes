// all models in this folder will be available
// as models.<ModelFileName>
const mongoose            = require('mongoose');
const fs                  = require('fs');
const logger              = require("../utils/Logger")

var models                = {};

mongoose.set('useCreateIndex', true);

models.mongoose           = mongoose;

// load all the models
logger.info("Loading all Models...");
var modelFiles = fs.readdirSync(__dirname);

for (var i=0; i<modelFiles.length; i++) {
  var fileName = modelFiles[i].split(".")[0];
  
  if (fileName !== "index") {
    models[fileName] = require(`./${fileName}`)(fileName);
  }
}

module.exports = models;
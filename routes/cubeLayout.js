const logger = require("../utils/Logger");
const CubeLayoutModel = require("../models/CubeLayout");

exports.getCubeLayout = async function(req, res) {
  var cube = res.locals.cube;
  var cubeId = cube.CubeID;
  var models = req.app.locals.models;
  var cube = await models.CubeLayout.find({
    CubeID: cubeId
  });
  res.status(200).send(JSON.stringify(cube));
};

exports.cubeLayoutCreate = async function(req, res) {
  var cube = res.locals.cube;
  var cubeId = cube.CubeID;
  var models = req.app.locals.models;
  var layout = await models.CubeLayout.find({
    layoutName: req.body.layoutName
  });
  if (layout.length == 0) {
    models.CubeLayout.create(
      {
        layoutName: req.body.layoutName,
        CubeID: cubeId
      },
      function(err, response) {
        if (err) {
          logger.error(err);
          return;
        }
        res.status(200).send({ success: true, data: response });
      }
    );
  } else {
    res
      .status(200)
      .send({ success: false, message: "Layout name already exists" });
  }
};

exports.addCubeDataParams = async function(req, res) {
  var cube = res.locals.cube;
  var cubeId = cube.CubeID;
  var models = req.app.locals.models;
  var layout = await models.CubeLayout.find({
    layoutName: req.body.layoutName
  });
  models.CubeLayout.updateOne(
    { "layoutName": req.body.layoutName },
    {
      $set: {
        dataParams: req.body.dataParams
      }
    },
    function(err, response) {
      if (err) {
        logger.error(err);
        return;
      }
      console.log(response)
      res.status(200).send({ success: true, data: response });
    }
  );
};

exports.addCubeGraphParams = async function(req, res) {
  var cube = res.locals.cube;
  var cubeId = cube.CubeID;
  var models = req.app.locals.models;
  var layout = await models.CubeLayout.find({
    layoutName: req.body.layoutName
  });
  models.CubeLayout.updateOne(
    { "layoutName": req.body.layoutName },
    {
      $set: {
        graphParams: req.body.graphParams
      }
    },
    function(err, response) {
      if (err) {
        logger.error(err);
        return;
      }
      console.log(response)
      res.status(200).send({ success: true, data: response });
    }
  );
};

const validator           = require('validator');

module.exports = async function (request, response, next) {
  var models = request.app.locals.models;
  var cubeId = request.params.cubeId;

  if (!validateCubeId(cubeId)) {
    response
    .status(400)
    .send({
      error   : true,
      message : "Invalid Cube Id"
    });
    return;
  }

  // look for this cube in the database
  var cube = await models.Cube.findOne({
    CubeID  : cubeId
  });
  
  if (!cube) {
    response
    .status(404)
    .send({
      error   : true,
      message : "Cube not found"
    });
    return;
  }

  // is this cube enabled?
  if (!cube.enabled) {
    response
    .status(403)
    .send({
      error   : true,
      message : "Cube disabled"
    });
    return;
  }

  // all good so far
  // make this cube document available in response.locals
  response.locals.cube = cube;

  next(); // move on to the next middleware
}

function validateCubeId (input) {
  return (validator.isHexadecimal(input))
}
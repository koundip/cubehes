const mongoose						= require('mongoose');

const Schema 							= mongoose.Schema;
const SchemaTypes 				= mongoose.SchemaTypes;

module.exports						= function (name) {

	var schema							= new Schema({
		CubeID								: {
      type                : String,
      required            : true,
      unique              : true,
      index               : true
    },
    enabled               : Boolean,
    name                  : String,
    description           : String,
    coordinates           : {
      latitude            : Number,
      longitude           : Number
    },
    knownEvents           : [String],
    busbarParams          : [String],
    feeders               : [String],
    feederParams          : [String],
    temp10Params          : [String],
    isOffline             : {
      type                : Boolean,
      default             : true
    },
    lastComm              : Date,
    idleTimeout           : { // seconds of no data will be declared offline
      type                : Number,
      default             : 300
    }
	}, {
		timestamps						: true // for createdAt, updatedAt atributes
	})

	return mongoose.model(name, schema);
}
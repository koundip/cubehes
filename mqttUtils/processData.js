const validator           = require('validator');
const moment              = require('moment');
const logger              = require("../utils/Logger")

function processEvent(locals,data) {

    var models = locals.models;
    var jsonData;
    //first check if it is json data!
    try {
        jsonData = JSON.parse(data);
      }
    catch(error) {
        logger.error("JSON parsing failed with error : " + error);
        return;
      }

    //now just check if there is a cubeId with it 
    if(jsonData['CubeId'] === undefined || jsonData['Feeders'] === undefined) {
        logger.error("One or more of the mandatory parameters is/are missing in the Json body!");
        return;
    }
    logger.info("Recieved new Data on the Event topic ts in data is  :" + jsonData["Feeders"][0]["ts"]);
    logger.debug(JSON.stringify(jsonData));
    var cubeId = jsonData['CubeId'];

    if (!validateCubeId(cubeId)) {
        logger.error("CubeId validation Failed!!");
        return;
      }

    models.Cube.findOne({CubeID : cubeId},(err,cube) => {

        if(err) {
            logger.error(err);
            return;
        }

        //good case
        if(cube === null) {
            logger.error("Cube Info doesnt exsist for this CubeID");
            return;
        }
        
        //has a valid cubeID now add it to the locals
        //remove the current event for this cube
        for(var i=0;i < locals.events.length;i++) {
            if(locals.events[i]["CubeId"] == cubeId) {
                locals.events.splice(i,1);
                break;
            }
        }

        locals.events.push(jsonData);
    });//findOne call Back


}
function processData(locals,data){
    var models = locals.models;
    var jsonData;
    //first check if it is json data!
    try {
        jsonData = JSON.parse(data);
    }
    catch(error) {
        logger.error("JSON parsing failed with error : " + error);
        return;
    }
    //it is json data now get the cubeID
    if(jsonData['CubeID'] === undefined || jsonData['Busbar'] === undefined || jsonData['Feeders'] === undefined) {
        logger.error("One or mroe of the mandatory parameters is/are missing in the Json body!");
        return;
    }

    logger.info("Recieved new Data on the data topic ts in data is :" + jsonData["Busbar"]["ts"]);
    logger.debug(JSON.stringify(jsonData));

    var cubeId = jsonData['CubeID'];

    if (!validateCubeId(cubeId)) {
        logger.error("CubeId validation Failed!!");
        return;
      }

    models.Cube.findOne({CubeID : cubeId},(err,cube) => {

        if(err) {
            logger.error(err);
            return;
        }

        //good case
        if(cube === null) {
            logger.error("Cube Info doesnt exsist for this CubeID");
            return;
        }

        //else cube id is good! store the record
        models.cubeData.create(jsonData,(err,res)=>{
            if(err) {
                logger.error(err.errmsg);
                return;
            }
            logger.info("Inserted a new Cube Data for CubeID = " + cubeId);

            //create the json to update the cube info
            var cubeInfo = {};

            cubeInfo["CubeID"] = cubeId;
            cubeInfo["busbarParams"] = Object.keys(jsonData["Busbar"]);

            //list of all feeder names!
            feeders = [];
            FeederArr = jsonData["Feeders"];
            for(var i=0; i< FeederArr.length;i++) {
                feeders.push(FeederArr[i].Name);
            }
            cubeInfo["feeders"] = feeders;
            cubeInfo["feederParams"] = Object.keys(jsonData["Feeders"][0]);
            cubeInfo["enabled"] = true;
            cubeInfo["isOffline"] = false;
            cubeInfo["lastComm"] = moment().toDate();

            //Update the last Data
            for(var i=0;i < locals.data.length;i++) {
                if(locals.data[i]["CubeID"] == cubeId) {
                    locals.data.splice(i,1);
                    break;
                }
            }
            locals.data.push(jsonData);

            //update the cubeInfo!!
            models.Cube.updateOne(cubeInfo,(err,data)=> {
                if(err) {
                    logger.error(err.errmsg);
                    return;
                } else {
                    logger.info("Updating the cube Info succeeded!!")
                }
            });//update Call back
        });//create callback
    });//findOne call Back
}
module.exports = function (topic,locals,data) {

    if(topic === "blr/cube/data") {
        return processData(locals,data);
    } else if(topic === "blr/cube/event") {
        return processEvent(locals,data);
    } else {
        logger.warning("Potential loss of data!! Data recieved on unknown topic!");
        return;
    }
}

function validateCubeId (input) {
    return (input.length === 16 && validator.isHexadecimal(input))
}